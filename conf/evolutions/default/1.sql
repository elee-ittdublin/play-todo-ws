# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table todo (
  id                            bigint not null,
  created_at                    timestamp,
  title                         varchar(255),
  complete                      boolean,
  user_id                       bigint,
  constraint pk_todo primary key (id)
);
create sequence todo_seq;

create table user (
  id                            bigint not null,
  auth_token                    varchar(255),
  email                         varchar(255),
  sha_password                  varbinary(255),
  fullname                      varchar(255),
  creation_date                 timestamp,
  constraint pk_user primary key (id)
);
create sequence user_seq;

alter table todo add constraint fk_todo_user_id foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_todo_user_id on todo (user_id);


# --- !Downs

alter table todo drop constraint if exists fk_todo_user_id;
drop index if exists ix_todo_user_id;

drop table if exists todo;
drop sequence if exists todo_seq;

drop table if exists user;
drop sequence if exists user_seq;

