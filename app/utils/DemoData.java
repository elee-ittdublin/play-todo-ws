package utils;

import models.*;
import play.Environment;
import play.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DemoData {

    public User user1;
    public User user2;

    public Todo todo1_1;
    public Todo todo1_2;

    public Todo todo2_1;

    @Inject
    public DemoData(Environment environment) {
        if (environment.isDev() || environment.isTest()) {
            if (User.findByEmailAndPassword("user1@demo.com", "password") == null) {
                Logger.info("Loading Demo Data");

                user1 = new User("user1@demo.com", "password", "User One");
                user1.save();

                todo1_1 = new Todo(user1, "user 1 todo 1");
                todo1_1.save();

                todo1_2 = new Todo(user1, "user 1 todo 2");
                todo1_2.save();

                user2 = new User("user2@demo.com", "password", "User Two");
                user2.save();

                todo2_1 = new Todo(user2, "user 2 todo 1");
                todo2_1.save();
            }
        }
    }

}