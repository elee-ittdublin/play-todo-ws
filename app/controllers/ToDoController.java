// http://stackoverflow.com/questions/4379485/restful-on-play-framework
// http://stackoverflow.com/questions/2342579/http-status-code-for-update-and-delete/2342589#2342589

package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import java.util.*;
import play.mvc.*;

import models.*;

// @Security.Authenticated(Secured.class)
public class ToDoController extends Controller {

    public Result getAllToDos() {

        List<Todo> todos = Todo.findAll();
        return ok(Json.toJson((todos)));
    }

    public Result getToDo(Long id) {
        Todo todo = Todo.find.byId(id);
        return todo == null ? notFound() : ok(Json.toJson(todo));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result createToDo() {
        JsonNode json = request().body().asJson();
        if(json == null) {
            return badRequest("Expecting Json data");
        }

        Todo newTodo = Json.fromJson(json, Todo.class);
        newTodo.save();

        return created(Json.toJson(newTodo));
    }

    public Result updateToDo(Long id) {
        Todo updateTodo = Json.fromJson(request().body().asJson(), Todo.class);

        updateTodo.setComplete(false);
        updateTodo.update();
        return ok(Json.toJson(updateTodo));
    }

    public Result deleteToDo(Long id) {
        Todo.find.byId(id).delete();
        return noContent();
    }
}
