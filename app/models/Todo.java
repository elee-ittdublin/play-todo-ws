package models;

import java.util.*;
import javax.persistence.*;

import com.avaje.ebean.Model;
import play.data.validation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;

// Product Entity managed by the ORM
@Entity
public class Todo extends Model {

    // Properties
    // Annotate id as the primary key
    @Id
    private Long id;

    // Other fields marked as being required (for validation purposes)
    private Date createdAt;

    @Constraints.MaxLength(1024)
    @Constraints.Required
    private String title;

    private Boolean complete;

    @ManyToOne
    @JsonIgnore
    private User user;


    // Default constructor
    public Todo() {
        this.createdAt = new Date();
        this.complete = false;
    }

    // Default constructor
    public Todo(User u, String title) {
        this.user = u;
        this.title = title;
        this.createdAt = new Date();
        this.complete = false;
    }

    //Generic query helper for entity Computer with id Long
    public static Finder<Long, Todo> find = new Finder<Long, Todo>(Todo.class);

    // Find all Products in the database
    // Filter product name
    public static List<Todo> findAll() {
        return Todo.find.all();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getComplete() {
        return complete;
    }

    public void setComplete(Boolean complete) {
        this.complete = complete;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}