package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
public class User extends Model {

    @Id
    private Long id;

    private String authToken;

    @Constraints.MaxLength(256)
    @Constraints.Required
    @Constraints.Email
    private String email;

    @Constraints.Required
    @Constraints.MaxLength(64)
    private byte[] shaPassword;

    @Transient // Don't persist this field
    @Constraints.Required
    @JsonIgnore
    private String password;

    @Constraints.Required
    @Constraints.MinLength(2)
    @Constraints.MaxLength(256)
    private String fullname;

    private Date creationDate;

    @OneToMany
    @JsonIgnore
    private List<Todo> todos = new ArrayList<Todo>();

    public User() {
        this.creationDate = new Date();
    }

    public User(String email, String password, String fullName) {
        setEmail(email);
        setPassword(password);
        setFullname(fullName);
        this.creationDate = new Date();
    }

    public static Finder<Long, User> find = new Finder<>(User.class);

    public static User findByAuthToken(String authToken) {
        if (authToken == null) {
            return null;
        }

        try  {
            return find.where().eq("authToken", authToken).findUnique();
        }
        catch (Exception e) {
            return null;
        }
    }

    public static User findByEmailAndPassword(String email, String password) {
        // todo: verify this query is correct.  Does it need an "and" statement?
        return find.where().eq("email", email.toLowerCase()).eq("shaPassword", getSha512(password)).findUnique();
    }


    public static byte[] getSha512(String value) {
        try {
            return MessageDigest.getInstance("SHA-512").digest(value.getBytes("UTF-8"));
        }
        catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String createToken() {
        this.authToken = UUID.randomUUID().toString();
        return authToken;
    }

    public void deleteAuthToken() {
        this.authToken = null;
        this.save();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email.toLowerCase();
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        this.shaPassword = getSha512(password);
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Date getCreationDate() {
        return creationDate;
    }


    public List<Todo> getTodos() {
        return todos;
    }

    public void setTodos(List<Todo> todos) {
        this.todos = todos;
    }
}
